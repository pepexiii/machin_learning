#! /usr/bin/env python3

# how to run:
#    pytest-3 matrixmult.py


import pytest
import numpy as np

def multiply(a, b):
    #Establish dimentions of all matrices (returned matrix in comment)
    nrow_a = len(a) #nrow_c
    nrow_b = len(b)
    ncol_a = len(a[0])
    ncol_b = len(b[0]) #ncol_c
    #Check dimentions and throw exception if neccessary
    if ncol_a!=nrow_b:
        raise ValueError('Inner matrix dimentions must be equal!')
    c = [] #declare
    #For each row of result matrix:
    for row_c in range(0,nrow_a):
        row = []
        #For each cell in given row of reslut matrix:
        for col_c in range(0,ncol_b):
            tmp = 0
            #dot product of appropriate vectors:
            for i in range(0,nrow_b):
                tmp = tmp + a[row_c][i]*b[i][col_c]
            row.append(tmp)
        c.append(row)
    return(c)
    pass


def multiplyWithNumpy(a, b):
    return(np.dot(np.array(a), np.array(b)))
    pass


testdata = [
    ([[3]], [[6]], [[18]]),
    ([[1, 2]],   [[3], [4]], [[11]]),
    ([[1, 2, 3], [4, 5, -7]],  [[3, 0, 4], [2, 5, 1], [-1, -1, 0]],  [[4, 7, 6], [29, 32, 21]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b,expected", testdata)
def test_multiply(multiplicationFunc, a, b, expected):
    assert np.array_equal(multiplicationFunc(a, b), expected)


testdataImpossibleProducts = [
    ([[3, 4]], [[6, 7]]),
    ([[1, 2]], [[3, 4], [4, 5], [5, 6]])
]
@pytest.mark.parametrize("multiplicationFunc", [multiply, multiplyWithNumpy])
@pytest.mark.parametrize("a,b", testdataImpossibleProducts)
def test_multiplyIncompatibleMatrices(multiplicationFunc, a, b):
    with pytest.raises(Exception) as e:
        multiplicationFunc(a, b)



