#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 15:37:11 2018

@author: lab
"""
#import os
#os.chdir('C:\\Users\\pepe\\workspace\\machin_learning\\01')

import numpy as np
import mnist_loader
import random
import scipy.special

class neur_net:
    def __init__(self, 
                 layers = np.zeros((1,1))):
        self.set_weights(layers)
        self.c = 0.2
    @staticmethod
    def sigma(inp,diff=0):
        if diff == 0:
            return(1/(np.exp(-inp)+1))
            #return(scipy.special.expit(inp))
        if diff ==1:
            return((1 - (1/(np.exp(-inp)+1)))/(np.exp(-inp)+1))
            #return(scipy.special.expit(inp)*(1-scipy.special.expit(inp)))
    def set_level_weighs(self, level, weights):
        self.layers[level] = weights
    def set_weights(self, weights):
        self.layers = []
        for i in range(0,len(weights)):
            self.layers.append(weights[i])
    def set_random_weights(self, dims):
        """dims is a list of ints - first element indicates number of inputs,
        last - number of outputs,
        any elemnt in between is number of neurons in respective layer"""
        self.layers = []
        for i in range(0,len(dims)-1):
            self.layers.append(np.random.rand(dims[i]+1,dims[i+1])*2-1)
    def get_response(self, inp):
        if len(inp) != self.layers[0].shape[0]-1:
            raise ValueError('Impropoer dimensions')
        for layer in self.layers:
            inp = np.concatenate([inp,[-1]])
            inp = self.sigma(np.dot((inp), layer))
        return(inp)
    def get_update(self, inp, des_output):
        deltas = []
        inps = []
        updates = []
        #if len(inp) != self.layers[0].shape[0]-1:
        #    raise ValueError('Impropoer dimensions')
        for layer in self.layers:
            inp = np.concatenate([inp,[-1]])
            inps.append(inp)
            inp = self.sigma(np.dot((inp), layer))
        """inp is now out"""
        err = (des_output - inp)[np.newaxis]
        delta = np.multiply(err,self.sigma(inp,1))
        deltas.append(delta)
        #self.new_layers[1] = (self.layers[1] + np.dot(inps[1][np.newaxis].T, self.c*delta))
        
        for i in reversed(range(1,len(self.layers))): 
            err = np.dot(self.layers[i],delta.T)
            err=err[0:len(err)-1].T
            inp=inps[i][0:len(inps[i])-1,]
            delta = np.multiply(err,self.sigma(inp,1))
            deltas.insert(0,delta)
        for i in range(0,len(self.layers)):
            updates.append(np.dot(inps[i][np.newaxis].T, self.c*deltas[i]))
            #self.layers[i] = (self.layers[i] + np.dot(inps[i][np.newaxis].T, self.c*deltas[i]))
        return(updates)
    def do_update(self, updates):
        for i in range(0,len(self.layers)):
            self.layers[i] = self.layers[i] + updates[i]
    def do_train_online(self, training_data, num_iter = 1):
        training_data = list(training_data)
        for j in range(0,num_iter):
            for i in random.sample(range(0, len(training_data)),len(training_data)):
                self.do_update(self.get_update([j[0] for j in training_data[i][0]], [j[0] for j in training_data[i][1]]))
    def do_test(self, test_data):
        correct = 0
        test_data2 = list(test_data).copy()
        l = len(test_data2)
        for expl in test_data2:
            if np.argmax(n.get_response([j[0] for j in expl[0]]))==expl[1]:
                correct = correct + 1
        return(correct/l)
    def do_train_batch(self, training_data, num_iter = 1, batch_size=25):
        training_data = list(training_data).copy()
        for k in range(0,num_iter):
            training_data = random.sample(training_data, len(training_data))
            for i in range(0,int(np.ceil(len(training_data)/batch_size))):
                updates = []
                for layer in self.layers:
                    updates.append(np.zeros([np.shape(layer)[0],np.shape(layer)[1]]))
                for expl in training_data[i*batch_size:min((i+1)*batch_size-1,len(training_data)-1)]:
                    update = self.get_update([j[0] for j in expl[0]], [j[0] for j in expl[1]])
                    for j in range(0,len(update)):
                        updates[j] = updates[j] + update[j]/batch_size
                self.do_update(updates)
            #print([i*batch_size,min((i+1)*batch_size-1,len(training_data))])
    def do_grid_par_validation(self, training_data, validation_data, cmin=0,cmax=1,ck=3,lsmin=10, lsmax=1500, lsk=3, layers=[784, 100, 10], iterations=1):
        acc = []
        #for c in list(set([int(x) for x in np.linspace(1,2,5)])):
        for c in list(np.linspace(cmin,cmax,ck)):
            self.set_random_weights(layers)
            self.do_train_online(training_data,1)
            acc.append(self.do_test(validation_data))
        return(acc)
        

"""example from pdf"""
"""
n = neur_net()
n.set_weights([np.array([[0.5, 0.2],[0.3, 0.7], [0.5, 0.5]]),
               np.array([[0.4, 0.4],[0.2, 0.9], [0.5, 0.5]])])
n.set_weights([np.array([[0.1, -0.4],[-0.2, 0.5], [0.3, -0.6]]),
               np.array([[0.15, -0.45],[-0.22, 0.55], [0.35, -0.65]])])
print(n.get_response([1,2]))
for i in range(0,10000):
   n.do_train([1,0], [1, 0])
   print(n.get_response([2,1]))
print(n.get_response([2,1]))
"""

"""uneven dims"""
"""
n = neur_net()
n.set_random_weights([5, 3, 2])
#print(n.layers)
print(n.get_response([2, 1, 2, 1, 2]))
for i in range(0,1000):
    n.do_train([2, 1, 2, 1, 2], [1,0])
print(n.get_response([2, 1, 2, 1, 2]))
"""

"""mnist data"""

"""
n = neur_net()
n.set_random_weights([784, 100, 10])

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()
n.do_train_batch(training_data,1)
print('Correct guesses: '+str(round(100*n.do_test(test_data),2))+'%')

    
    
n = neur_net()
n.set_random_weights([784, 100, 10])

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()    
n.do_train_online(training_data,1)    
print('Correct guesses: '+str(round(100*n.do_test(test_data),2))+'%')    
"""

training_data, validation_data, test_data = mnist_loader.load_data_wrapper()    
n = neur_net()
n.do_grid_par_validation(training_data, validation_data)